import time
from threading import Thread
from multiprocessing import Process
from concurrent.futures import ThreadPoolExecutor


def ask_user():
    start = time.time()
    user_input = input("Enter your name: ")
    greet = f'Hello, {user_input}'
    print(f'\n{greet}')
    print(f'ask_user, {time.time() - start}')


def complex_calculation():
    start = time.time()
    print('Start calculating...')
    [x**2 for x in range(2000000)]
    print(f'complex_calculation, {time.time() - start}')


# start = time.time()
# ask_user()
# complex_calculation()
# print(f'Single thread total time: {time.time() - start}')

thread1 = Thread(target=complex_calculation)
thread2 = Thread(target=ask_user)
start = time.time()

thread1.start()
thread2.start()

thread1.join()
thread2.join()
print(f'Two threads total time: {time.time() - start}')


# now cleaning up the previous code by using the ThreadPool
print('\n Auto managed thread pool...')
pool_start_time = time.time()
with ThreadPoolExecutor(max_workers=2) as pool:
    pool.submit(complex_calculation)
    pool.submit(ask_user)

print(f'Thread pool total time: {time.time() - start}')


# Now using multiprocessing where we want complex calculation
# to happen and we do not have idle time like the
# ask_user function does.
process1 = Process(target=complex_calculation)
process2 = Process(target=complex_calculation)
process1.start()
process2.start()

process_start = time.time()
process1.join()
process2.join()

print(f'Two process total time: {time.time() - process_start}')
