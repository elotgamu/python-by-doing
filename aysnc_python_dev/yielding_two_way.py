from collections import deque

friends = deque(('Rolf', 'Jose', 'Charlie', 'Jen', 'Anna'))


def get_friend():
    yield from friends


# c = get_friend()
# print(next(c))
# print(next(c))

#  Receives a generator and yield
#  the current value as a salutation
#  message inside a while
#  the fact of yielding allows to resume
# execution
def greet(g):
    while True:
        try:
            friend = next(g)
            yield f'Hello {friend}'
        except StopIteration:
            print('On iteration stop')


friends_generator = get_friend()
g = greet(friends_generator)

print(next(g))
print(next(g))
