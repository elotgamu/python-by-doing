import time
import random
import queue

from threading import Thread

# Global counter
counter = 0

# things to be printed out
job_queue = queue.Queue()

# amounts by which to increase counter
counter_queue = queue.Queue()


def increments_manager():
    global counter

    while True:

        # this waits until an item is
        # available and then locks the
        # queue
        increment = counter_queue.get()
        old_counter = counter
        time.sleep(random.random())
        counter = old_counter + increment
        time.sleep(random.random())
        job_queue.put((f'New counter value is {counter}', '-----'))
        time.sleep(random.random())
        # unlock the counter queue
        counter_queue.task_done()


def print_manager():

    while True:
        for line in job_queue.get():
            print(line)
        job_queue.task_done()


Thread(target=increments_manager, daemon=True).start()
Thread(target=print_manager, daemon=True).start()


def increment_counter():
    counter_queue.put(1)


worker_threads = [Thread(target=increment_counter) for thread in range(10)]

for thread in worker_threads:
    thread.start()

for thread in worker_threads:
    thread.join()

counter_queue.join()
job_queue.join()
