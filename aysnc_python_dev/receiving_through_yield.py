from collections import deque

friends = deque(('Rolf', 'Jose', 'Charlie', 'Jen', 'Anna'))

# ask for the greeting salutation
# using a yield which will pause the execution
# until we receive something via `send` method


def friends_upper():
    while friends:
        friend = friends.popleft().upper()
        greeting = yield
        print(f'{greeting} {friend}')


# this code could be replaced
# whit a simpler `yield from g`
# but were exploring how is this
# implemented
def get_greetings(g):
    g.send(None)

    while True:
        greeting = yield
        g.send(greeting)


greeter = get_greetings(friends_upper())
greeter.send(None)
greeter.send('Hello')

# we print somethig unrelated
print('Hello world')
# and now priming the generator resume
# where it was left
greeter.send('How are you,')
