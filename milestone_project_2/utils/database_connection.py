import sqlite3


class DatabaseConnection:
    def __init__(self, database):
        self.database = database
        self.connection = None

    def __enter__(self) -> sqlite3.Connection:
        self.connection = sqlite3.connect(self.database)
        return self.connection

    def __exit__(self, exc_type, exc_val, exc_tb):

        if exc_type or exc_val or exc_tb:
            print(f'Closing connection due to {exc_type} happened!')
            self.connection.close()
            return

        print("Committing changes to database (if any)")
        self.connection.commit()
        self.connection.close()
