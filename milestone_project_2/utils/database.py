"""
Concerned with storing and retrieving books from a csv file

Format of the file is:
name,author,read
"""
import os
import string
from typing import Dict, List, Union

from .database_connection import DatabaseConnection

# type definition for book interface
Book = Dict[str, Union[str, int]]

BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')
database = BASE_DIR + "/data.db"


def create_book_table() -> None:
    with DatabaseConnection(database) as connection:
        cursor = connection.cursor()
        cursor.execute(
            'CREATE TABLE IF NOT EXISTS \
                books(name text primary key, author text, read integer)'
        )


def add_book(name: str, author: str) -> None:
    with DatabaseConnection(database) as connection:
        cursor = connection.cursor()
        cursor.execute(
            'INSERT INTO books VALUES(?, ?, 0)', (name, author))


def get_all_books() -> List[Book]:
    with DatabaseConnection(database) as connection:
        cursor = connection.cursor()
        results = cursor.execute(
            'SELECT * FROM books')
        data = results.fetchall()

    books = []
    for book_data in data:
        name, author, read = book_data
        books.append({"name": name, "author": author, "read": read})
    return books


def list_books() -> None:
    books = get_all_books()
    for book in books:
        name = book["name"]
        author = book["author"]
        read = book["read"]
        isRead = 'Yes' if read == 1 else 'No'
        print(f"Name: {name}, Author: {author}, read: {isRead}")


def mark_book_as_read(name: str) -> None:
    with DatabaseConnection(database) as connection:
        cursor = connection.cursor()
        row_count = cursor.execute(
            'UPDATE books SET read=1 WHERE name=?', (name,)).rowcount

    print(f'Marking books as read. {row_count} affected.')


def delete_book_by_name(name: str) -> None:
    with DatabaseConnection(database) as connection:
        cursor = connection.cursor()
        row_count = cursor.execute(
            'DELETE FROM books WHERE name=?', (name,)).rowcount

    print(f'Deleting book. {row_count} affected.')
