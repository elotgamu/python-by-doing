from utils import database

USER_CHOICE = """
Enter:
- 'a' to add a new book
- 'l' to list all books
- 'r' to mark a book as read
- 'd' to delete a book
- 'q' to quit

Your choice: """


valid_choices = {
    "a": "",
    "l": "",
    "r": "",
    "d": "",
    "q": ""
}


def menu():
    database.create_book_table()
    user_input = input(USER_CHOICE)

    while user_input != 'q':

        if user_input == 'a':
            prompt_add_book()
        elif user_input == 'l':
            list_books()
        elif user_input == 'r':
            prompt_read_book()
        elif user_input == 'd':
            prompt_delete_book()
        else:
            print("Unknown command. Please try again.")

        user_input = input(USER_CHOICE)


def prompt_add_book():
    name = input("\nEnter the name of the book: ")
    author = input("\n Enter the author of the book: ")
    database.add_book(name, author)


def list_books():
    database.list_books()


def prompt_read_book():
    name = input("\nEnter the name of the book: ")
    database.mark_book_as_read(name)


def prompt_delete_book():
    name = input("\nEnter the name of the book you want to delete: ")
    database.delete_book_by_name(name)


if __name__ == "__main__":
    menu()
