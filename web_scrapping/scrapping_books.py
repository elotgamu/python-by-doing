import requests
from bs4 import BeautifulSoup

page = requests.get('http://www.example.com')
page_instance = BeautifulSoup(page.content, 'html.parser')

print(page_instance.find('h1').string)
print(page_instance.select_one('p a').attrs['href'])
