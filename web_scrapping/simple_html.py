from bs4 import BeautifulSoup

SIMPLE_HTML = """<html>
<head></head>
<body>
<h1>This is a title</h1>
<p class="subtitle">Lorem ipsum dolor sit amet. Consectetur edipiscim elit.</p>
<p>Here's another p without a class</p>
<ul>
    <li>Rolf</li>
    <li>Charlie</li>
    <li>Jen</li>
    <li>Jose</li>
</ul>
</body>
</html>"""

simple_soup = BeautifulSoup(SIMPLE_HTML, 'html.parser')


def find_title():
    h1_tag = simple_soup.find('h1')
    print(h1_tag.string)


def find_list_items():
    list_items = simple_soup.find_all('li')
    list_content = [item.string for item in list_items]
    print(list_content)


def find_subtitle():
    subtitle = simple_soup.find('p', {'class': 'subtitle'})
    print(subtitle.string)


def find_non_class_paragraphs():
    all_paragraphs = simple_soup.find_all('p')
    non_class_paragraphs = [
        p.string for p in all_paragraphs if p.attrs.get('class') is None]
    print(non_class_paragraphs)


find_title()
find_list_items()
find_subtitle()
find_non_class_paragraphs()
