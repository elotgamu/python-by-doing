friends_last_seen = {
    'Rolf': 31,
    'Jen': 1,
    'Anne': 7
}

# another_variable = friends_last_seen
print(id(friends_last_seen))

friends_last_seen = {
    'Rolf': 31,
    'Jen': 1,
    'Anne': 7
}

print(id(friends_last_seen))

# friend_last_seen.__setitem__(self, 'Rolf', 0)
friends_last_seen['Rolf'] = 0

# however modifying a integer returns a new object
my_int = 0
print(f"mem addr for my_int: {id(my_int)}")
my_int += 1
print(f"mem addr for my_int + 1 : {id(my_int)}")


"""
integers -> all functions returns a new int
"""
