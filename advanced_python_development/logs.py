import logging

logger = logging.getLogger('test_logger')

"""
DEBUG
INFO
WARNING
ERROR
CRITICAL
"""

# info type logs will not be displayed by default
logger.info('This will not show up')

# this will be displayed as loggers display log of level warning and up
logger.warning('This will show up.')


# to modify the default initial log level we should set the logging level
# at log creation

# we can also add thing to logger by modifying the format params
logging.basicConfig(
    format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)
debugLogger = logging.getLogger('debug_logger')

# this info level will be printed
debugLogger.info("An info logger tha will show up")

# we can customize the logging info deeply like
logging.basicConfig(
    format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    level=logging.DEBUG, filename='logs.txt')
customizedLogger = logging.getLogger('debug_logs')
customizedLogger.info('A very customized log will display here...')
customizedLogger.critical('An error ocurred ')
