from collections import Counter, defaultdict, OrderedDict, namedtuple, deque

"""
* counter
Create a counter for hashable items
you can pass the value and it will return
how many occurrences it found
"""
device_temperatures = [13.5, 14.0, 14.0, 14.5, 14.5, 14.5, 15.0, 16.0]
temperature_counter = Counter(device_temperatures)
#
print(f"There are {temperature_counter[14.5]} items with temperature of 14.5")

"""
* defaultdict
"""

coworkers = [
    ('Rolf', 'MIT'),
    ('Jen', 'Oxford'),
    ('Rolf', 'Cambridge'),
    ('Charlie', 'Manchester'),
]

alma_maters = {}

# we could store and group our data
# looping through elements like
for person, alma_mater in coworkers:
    if person not in alma_maters:
        alma_maters[person] = []

    alma_maters[person].append(alma_mater)

print(f"person alma maters are {alma_maters}")

# however we can also do
universities = defaultdict(list)
for person, university in coworkers:
    universities[person].append(university)

print(f"coworkers attended the following universities {universities}")

"""
* Ordered Dict
"""

o = OrderedDict()
o['Rolf'] = 6
o['Jose'] = 12
o['Jen'] = 3

o.move_to_end('Rolf')
o.move_to_end('Jen', last=False)

print(f"Ordered dict {o}")

# remove last item
o.popitem()
print(f"ordered dict without last item {o}")

"""
* namedtuple
"""

account = ('checking', 1850.90)

Account = namedtuple('Account', ['name', 'balance'])
account = Account('checking', 1850.90)
# we can now access properties like a a dict
print(f"account name {account.name}, account balance {account.balance}")


"""
* deque: Double end queue
"""

friends = deque(('Rolf', 'Charlie', 'Jen', 'Anna'))

# add element as last item
friends.append('Jose')
# add to the beginning Adding anthony before Rolf
friends.appendleft('Anthony')
print(friends)

# remove last item
friends.pop()
# remove first item (removing last from right to left)
# removing anthony
friends.popleft()
print(friends)
