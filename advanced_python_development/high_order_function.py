

movies = [
    {"name": "The Matrix", "director": "Wachowski"},
    {"name": "A beautiful Day in the Neighborhood", "director": "Heller"},
    {"name": "The Irishman", "director": "Scorsese"},
    {"name": "Klaus", "director": "Pablos"},
    {"name": "1917", "director": "Mendes"},
]

# a high order function that will call
# the finder param


def find_movie(expected, finder):
    for movie in movies:
        if (finder(movie) == expected):
            return movie


find_by = input("What property are we searching by? ")
looking_for = input("What are we looking for? ")

# the finder param is a lambda to allow params or return values
movie = find_movie(looking_for, lambda movie: movie[find_by])
print(movie or 'No movies found')
