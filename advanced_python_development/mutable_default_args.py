
# This is a bad idea,
# mutable default args
# can have unexpected
# side effects
# across execution

def create_account(name: str, holder: str, account_holders: list = []):
    account_holders.append(holder)
    return {
        'name': holder,
        'main_account_holder': holder,
        'account_holders': account_holders
    }


a1 = create_account('checking', 'Rolf')
a2 = create_account('savings', 'Jen')

print(f"a1 {a1}")
print(f"a2 {a2}")
