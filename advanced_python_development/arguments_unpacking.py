accounts = {
    'checking': 1958.00,
    'savings': 3695.59
}


def add_balance(amount: float, name: str = 'checking') -> float:
    """
    Function to update the balance of
    an account and return the new balance
    """
    accounts[name] += + amount
    accounts[name]


transactions = [
    (-189.67, 'checking'),
    (-220.00, 'checking'),
    (220.00, 'savings'),
    (-15.70, 'checking'),
    (-23.90, 'checking'),
    (-13.00, 'checking'),
    (1579.50, 'checking'),
    (-600.50, 'checking'),
    (600.50, 'savings'),
]

for transaction in transactions:
    # can do this (passing tuple item by index)
    # add_balance(transaction[0], transaction[1])
    #
    # we can also pass named args
    # add_balance(amount=transaction[0], name=transaction[1])
    #
    # but the pythonic method is to unpack them
    add_balance(*transaction)


# Dictionary unpacking


# Given the User class
class User:

    # init method
    def __init__(self, username, password):
        self.username = username
        self.password = password


# imagine these comes from out DB
# notice the dictionary keys match
# the props expected by the User class
# init method
users = [
    {'username': 'elotgamu', 'password': '123'},
    {'username': 'rboza', 'password': '456'},
]

# we pass an dictionary unpacked
# where each prop will be the ones in the
# data dict
user_objects = [User(**data) for data in users]
