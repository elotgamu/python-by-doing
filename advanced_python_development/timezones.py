from datetime import datetime, timedelta, timezone

# time in "naive" format (not tz aware)
print("current time in OS is ", datetime.now())

# time with tz aware (UTC)
print("time is ", datetime.now(timezone.utc))

# getting tomorrow date using time object difference
# timedelta fn
today = datetime.now(timezone.utc)
tomorrow = today + timedelta(days=1)
print(f"today is {today} and tomorrow will be {tomorrow}")

# format date time
print(today.strftime('%d-%m-%Y %H:%M:%S'))

# ask for user input
user_date = input('\nEnter the date in YYYY-MM-DD format: ')
parsed_date = datetime.strptime(user_date, '%Y-%m-%d')
print("parsed date is ", parsed_date)
