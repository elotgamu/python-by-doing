import time
import timeit


def powers(limit):
    return [number**2 for number in range(limit)]


def measure_runtime(func):
    start = time.time()
    func()
    end = time.time()
    print(f"function took {end - start} to execute")


# print(power(5))
measure_runtime(lambda: powers(50000000))

# python built-in fn for timing execution
# it executes code many time to return the average time
print(timeit.timeit("[number**2 for number in range(10)]"))
