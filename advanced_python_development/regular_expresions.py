import re

email = 'jose@tecladocode.com'
expression = '[a-z]+@[a-z]+\.[a-z]+'

matches = re.findall(expression, email)

print(f"matches pattern {matches}")

price = 'Price: $1,800.50'
pattern = 'Price: \$([0-9,]*\.[0-9]*)'

matches = re.search(pattern, price)

amount_text = matches[1].replace(",", "")
print(f"match => {matches[0]}")
print(f"found => {matches[1]}")
print(f"amount found is ${float(amount_text)}")
