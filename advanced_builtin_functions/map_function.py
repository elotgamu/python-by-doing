friends = ['Rolf', 'Jose', 'Randy', 'Anna', 'Mary']

friends_lower = map(lambda x: x .lower(), friends)
friends_lower_list_comprehension = [
    friend.lower for friend in friends
]
friends_lower_generator_comprehension = (
    friend.lower() for friend in friends
)

print(f'friend lower map => {list(friends_lower)}')
print(
    f'friends lower generator comprehension \
        {list(friends_lower_generator_comprehension)}')
