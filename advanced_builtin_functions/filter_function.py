def start_with_r(friend):
    return friend.startswith('R')


friends = ['Rolf', 'Jose', 'Randy', 'Anna', 'Mary']
friends_with_r = filter(start_with_r, friends)

friends_with_r_lambda = filter(lambda friend: friend.startswith('R'), friends)

# we can also achieve the same with generator comprehension
friends_from_generator = (f for f in friends if f.startswith('R'))

print(f'friends => {friends}')
print(f'friends with R => {list(friends_with_r)}')
print(f'friends with R lambda => {list(friends_with_r_lambda)}')
print(f'friend with R (generator) => {list(friends_from_generator)}')


# for completeness sake we can create
# a custom `filter` method
def my_custom_filter(func, iterable):
    for i in iterable:
        if func(i):
            yield i


friends_r_custom_filter = my_custom_filter(start_with_r, friends)
print(f'friends with r custom filter fn => {list(friends_r_custom_filter)}')
