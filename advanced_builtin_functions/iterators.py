class Iterable:
    def __iter__():
        """
            By defining the built in __iter__ method
            python treats the class as an iterable
        """
        pass


class AnotherIterable:
    """
        This class defines two
        built in methods that also
        tells python that this is
        an iterable
        __len__ and __getitem__

    """

    def __init__(self) -> None:
        self.cars = ['Fiesta', 'Focus']

    def __len__(self):
        """Used to return the len of the class"""
        return len(self.cars)

    def __getitem__(self, i):
        """ Defined to be able to get at item at an specific index"""
        return self.cars[i]


cars = AnotherIterable()

# this will print cars length
# by making use of the __len__ method
print(f"### Cars has {len(cars)} items\n")

# this will print every item
# on the cars instance
# this is because we defined
# the behaviour of the __getitem__ method
for car in cars:
    print(f"car is {car}\n")
