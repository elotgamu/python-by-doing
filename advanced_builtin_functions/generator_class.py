
class FirstHundredNumberGenerator:
    def __init__(self) -> None:
        self.number = 0

    def __next__(self):
        """ By Defining the __next__ method
            our class is now iterable
            so python see this class
            as an iterator

            All generator are iterator by definition
        """
        if self.number < 100:
            current = self.number
            self.number += 1
            return current
        else:
            raise StopIteration()


my_gen = FirstHundredNumberGenerator()

print(next(my_gen))
print(next(my_gen))


# Define a PrimeGenerator class
class PrimeGenerator:
    # You may modify the __init__() method if necessary,
    # but you don't need to change its arguments
    def __init__(self, stop):
        # stop defines the range (exclusive upper bound)
        # in which we search for the primes
        self.stop = stop
        self.start = 2

    def __next__(self):
        for n in range(2, self.stop):
            for x in range(2, n):
                if n % x == 0:
                    break
            else:
                self.start = n + 1
                return n
        raise StopIteration()
