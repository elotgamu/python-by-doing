"""
Define a generator to return
0 - 99 numbers.

A generator does return the result
and populates memory. It instead returned
result form the last call so every time
we call `next` it will return the next value

This is good for big amount of data
"""


def hundred_numbers():
    """ Define a generator to return 0 - 99 numbers."""
    i = 0
    while i < 100:
        yield i
        i += 1


# store generator reference
g = hundred_numbers()

# this does not print
# the whole list, it
# instead return the
# __repr__ like:
# <generator object <function_name> at <memory_location>
print(g)

# display first two values
print(next(g))  # print 0
print(next(g))  # print 1

# however the list method return the whole content
# but we need a new reference
# because `g` will not contains first two calls
# because they were already `returned`
gen = hundred_numbers()
print(list(gen))

# and print over the next for gen throws a
# StopIteration error since the generator
# already returned all values
print(next(gen))
