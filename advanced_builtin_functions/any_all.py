friends = [
    {'name': 'Lea Verou', 'location': 'Greece'},
    {'name': 'Maggie Appleton', 'location': 'England'},
    {'name': 'Dan Abramov', 'location': 'England'},
    {'name': 'Talia Perez', 'location': 'USA'},
]

your_location = input("Please input a location: ")
nearby_friends = [
    friend for friend in friends if friend['location'] == your_location]

# any evaluates truthiness
# being 0, [], (), False, None all falsy values
if any(nearby_friends):
    print(
        f'You are not alone. There are {len(nearby_friends)} friend(s) nearby')
