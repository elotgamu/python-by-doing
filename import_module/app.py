import os

from utils.file_operations import read_file, save_to_file
# from utils.find import find_in


file_dir = os.path.dirname(os.path.abspath(__file__))
data_file_path = file_dir + "/data.txt"

save_to_file('Rolf', data_file_path)

content = read_file(data_file_path)
print(content)

print(__name__)
