def find_in(iterable, finder, expected):
    for i in iterable:
        if finder(i) == expected:
            return i

    raise NotFoundError(f'{expected} not found in provided iterable.')


class NotFoundError(Exception):
    pass


# this tells python that if the file is run
# as script we can tell them which default
# function to run
if __name__ == '__main__':
    names = ['Rolf', 'Jose', 'Jen']
    print(find_in(names, lambda x: x, 'Jose'))
