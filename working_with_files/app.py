my_file = open('data.txt', 'r')
file_content = my_file.read()

# very important to close
# the file to save resources
my_file.close()

print(file_content)

user_name = input('Enter your name: ')

# w mode replaces the current content
my_file_writtable = open('data.txt', 'w')
my_file_writtable.write(user_name)
my_file_writtable.close()
