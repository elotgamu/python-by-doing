# Ask the user for a list of 3 friends
# For each friend, we'll tell the user whether they are nearby
# For each nearby friend, we'll save their name to the `nearby_friend.txt`


# hint: readlines()

friends = input(
    'Enter three friend names, separated by commas (no spaces, please)')

friends = friends.split(",")

people_file = open('people.txt', 'r')
people_nearby = [line.strip("\n") for line in people_file.readlines()]
people_file.close()

friends_set = set(friends)
people_nearby_set = set(people_nearby)

nearby_friends = friends_set.intersection(people_nearby_set)

nearby_friends_file = open('nearby_friends.txt', 'w')

print(f"nearby friends {nearby_friends}")

for friend in nearby_friends:
    print(f"{friend} is nearby! Meet up with them.")
    nearby_friends_file.write(f'{friend}\n')

nearby_friends_file.close()

# with open('people.txt', encoding='utf8') as file:
#     content = file.read()
#     print(f"content is: \n{content}")

# file.close()
