import os

path = os.path.dirname(os.path.abspath(__file__))
print(path)
file_path = path + "/csv_data.csv"
file = open(file_path, 'r')
lines = file.readlines()
file.close()

data_content = lines[1:]
lines = [line.strip("\n") for line in data_content]


for line in lines:
    person_data = line.split(",")
    name, age, degree, university = person_data

    print(f"{name.title()} is {age}, studying {degree.capitalize()} at \
            {university.title()}")
