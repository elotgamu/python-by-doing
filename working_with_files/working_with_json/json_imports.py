import os
import json

folder_path = os.path.dirname(os.path.abspath(__file__))
file_path = folder_path + "/friends.json"

# open a file using a Context Manager
with open(file_path, 'r') as file:

    # this loads the json content
    # into a dictionary
    file_content = json.load(file)

print(file_content)

# ok, now lets feed a file
# with a json content

cars = [
    {'make': 'Ford', 'model': 'Fiesta'},
    {'make': 'Ford', 'model': 'Focus'}
]

json_file_path = folder_path + "/cars.json"
with open(json_file_path, 'w') as cars_file:
    json.dump(cars, cars_file)

# convert a json string into a python data type
my_json_string = '[{"name": "Alfa Romeo", "released": 1950}]'

incorrect_car = json.loads(my_json_string)
print(incorrect_car)
print(incorrect_car[0]["name"])
