"""
sample `questions.txt` file:
1+1=2
2+2=4
8-4=4
task description:
- read from `questions.txt`
- for each question, print out the question and and wait for the user's answer
  for example, for the first question, print out: `1+1=`
- after the user answers all the questions, calculate her score and write it
  to the `result.txt` file
  the result should be in such format: `Your final score is n/m.`
    where n and m are the number of correct answers and the maximum score
    respectively
"""

exercise_file = open('questions.txt', 'r')

exercise_items = [exercise.strip("\n")
                  for exercise in exercise_file.readlines()]
exercise_file.close()

print(f"exercise items f{exercise_items}")

quiz_results = []

for exercise in exercise_items:
    question, answer = exercise.split('=')

    user_prompt = f"\nWhat is the result of {question} ?: "
    user_answer = input(user_prompt)

    result = {'question': question, "answer": answer,
              "user_answer": user_answer, "correct": answer == user_answer}
    quiz_results.append(result)

print(f"quiz results {quiz_results}")

correct_answers = [
    result for result in quiz_results if result["correct"] is True]

with open(file='result.txt', mode='w', encoding="utf-8") as result_file:
    grade = (
        f"Your final score is "
        f"{len(correct_answers)}/{len(exercise_items)}."
    )
    result_file.write(grade)

print("Saving grade to result.txt file... Success")
result_file.close()
