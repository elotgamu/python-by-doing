# Please read the instructions carefully and write your script here:
# You need to:
# - read data from csv_file.txt
# - process data and convert them into a single JSON object
# - store the JSON object into json_file.txt
# Your code starts here:

# In this exercise, we ask you to create a CSV to JSON converter that can be
# handy for others. You converter should achieve the following tasks:

# Given a CSV file csv_file.txt with following format:

# Manchester United,Manchester,UK
# Real Madrid,Madrid,Spain
# Juventus,Turin,Italy
# Read and process the file and store its content in JSON format
#  into a `json_file.txt``.
# The according keys to each field in the CSV file are:
#  - club
#  - city
#  - country
# Thus the output should be, according to the given sample CSV file, like this:

# [{"club": "Manchester United", "country": "UK", "city": "Manchester"},
# {"club": "Real Madrid", "country": "Spain", "city": "Madrid"}, {"club":
# "Juventus", "country": "Italy", "city": "Turin"}]
# P.S. You can assume that the csv_file.txt file exists, and its data is
# always in proper format. It doesn't matter if key value pairs are in
# different order.

# Happy coding!

import os
import json

dir_path = os.path.dirname(os.path.abspath(__file__))

csv_file_path = dir_path + "/csv_file.txt"

csv_file = open(csv_file_path, 'r')
content = csv_file.readlines()
csv_file.close()

print(content)

parsed_content = [line.strip("\n").split(",") for line in content]
print(parsed_content)

json_content = []

for line in parsed_content:
    club, city, country = line
    team_data = {"club": club, "country": country, "city": city}
    json_content.append(team_data)

print(json_content)

json_file_path = dir_path + "/json_file.txt"
fp = open(json_file_path, 'w')
json.dump(json_content, fp)
fp.close()
